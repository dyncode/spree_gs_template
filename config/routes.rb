Spree::Core::Engine.routes.draw do
	devise_scope :user do
		match "mc-subscribe", :to => "user_registrations#subscribe", :as => :subscribe, :via => :post
	end
end
