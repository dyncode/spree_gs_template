Spree::BaseHelper.module_eval do
  CONVERSION_TYPES = [:conversion]
  
  def conversion_flash
    html = ""
    CONVERSION_TYPES.each do |ft|
      html << "#{h(flash[ft.to_sym].html_safe).html_safe}".html_safe unless flash[ft.to_sym].blank?
    end
    conversion = "#{raw html}"
    return conversion.html_safe if !flash.blank? || flash == ""
  end
end